import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

import { createStore, combineReducers } from 'redux'
import { Provider } from 'react-redux'

const reducerNumero = (state = 2, action) => {
    var newState = Object.assign({}, state)
    if(action.type === 'AUM'){
        newState = state + 1
        return newState
    }else if(action.type === 'DIS'){
        newState = state - 1
        return newState
    }
    return state
}

const reducerTareas = (state = [], action) => {
    var newState = Object.assign({}, state)
    if(action.type === 'ADD'){
        newState = state.concat({tarea: action.tarea, id: action.id})
        return newState
    }
    return state
}

const reducerId = (state = 1, action) => {
    var newState = Object.assign({}, state)
    
    if(action.type === 'ADD'){
        newState = state + 1
        return newState
    }
    
    return state
}

// combineReducers  toma un objeto JS con los demas reducers como valores
const reducer = combineReducers({
    numero: reducerNumero,
    tareas: reducerTareas,
    id: reducerId
})



// const state = {
//     cantidad: 2
// }

const store = createStore(reducer)

ReactDOM.render(
// I. Implementar el PROVIDER
<Provider store={ store }>
    <App />
</Provider>
, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
