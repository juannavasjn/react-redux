import React, { Component } from 'react';
import './App.css'

import { connect } from 'react-redux'

class App extends Component {
  agregarTarea = (evento) => {
    if(evento.which === 13){
      this.props.agregar(evento.target.value, this.props.id)
    }
    
  }
  render = () => {
    const elementosTareas = this.props.tareas.map( tarea => {
      return <h2 key={tarea.id}> {tarea.tarea}</h2>
    })
    return (
      <div className="App">
        {this.props.data}
        <br/>
        <button onClick={this.props.aumentar}>Aumentar</button>
        <button onClick={this.props.disminuir}>Disminuir</button>
        <br/>
        <input onKeyPress = {this.agregarTarea}/>
        <br/>
        {elementosTareas}
        <br/>
      </div>
    );
  }
}

// Ingresa como props a nuestro Component tanto el STATE
// como los DISPATCH

// internamente hace una subscripcion y un get state
// por lo que constantemente en caso de un cambio en el STATE se actualiza
// o se ejecute nuevamente

const mapStateToProps = state => {
  return {
    data: state.numero,
    id: state.id,
    tareas: state.tareas
  }
} 

// Es un objeto que las funciones internas son ACTION CREATOR 
// y que al ingresarlas a nuestro Component las engloba en DISPATCH para que de esta forma puedan ser llamadas como un DISPATCH
// const mapDispatchToProps = {
//   aumentar: () =>{ return {type: 'AUM'}},
//   disminuir: () =>{ return {type: 'DIS '}}
// }

// Tambien puede ser una funcion, 
const mapDispatchToProps = dispatch => {
  return {
    aumentar: () => {
      dispatch({type: 'AUM'})
    },
    disminuir: () => {
      dispatch({type: 'DIS'})
    },
    agregar: (tarea, id) => {
      dispatch({type: 'ADD', tarea, id})
    }
  }
}

// connect nos permite acceder al STATE y hacer dispatch
// de ACTIONS y ACTION CREATOR
export default connect(mapStateToProps, mapDispatchToProps)(App);
